<?php

class CarbonoZero_Shipping_Model_Observer
{
    public function adminhtmlWidgetContainerHtmlBefore($event) {
        $block = $event->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $order = $block->getOrder();

            $shippingMethod = $order->getShippingMethod(true);

            if ($shippingMethod != null
                && $shippingMethod->carrier_code == "CarbonoZero") {
                $block->removeButton('order_ship');

                if ($order->canShip() && !$order->hasShipments()) {
                    $orderId = $order->getId();
                    $label = Mage::helper('CarbonoZero_Shipping')->__('Create OS CarbonoZero');
                    $message = Mage::helper('CarbonoZero_Shipping')->__('Are you sure you want to create a shipping order?');
                    $url = Mage::helper('adminhtml')->getUrl('carbonozeroshipping/index/createOS', array('order_id'=>$orderId));
                    $block->addButton('create_os_carbono', array(
                        'label' => $label,
                        'class'     => 'go',
                        'onclick' => "confirmSetLocation('{$message}', '{$url}')"
                    ));
                }
            }
        }
        
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Shipment_View) {
            $shipment = $block->getShipment();
            $order = $shipment->getOrder();

            $shippingMethod = $order->getShippingMethod(true);

            if ($shippingMethod != null && $shippingMethod->carrier_code == "CarbonoZero") {
                $shipmentId = $shipment->getId();
                $label = Mage::helper('CarbonoZero_Shipping')->__('Cancel OS CarbonoZero');
                $message = Mage::helper('CarbonoZero_Shipping')->__('Are you sure you want to cancel this shipping order?');
                $url = Mage::helper('adminhtml')->getUrl('carbonozeroshipping/index/cancelOS', array('shipment_id'=>$shipmentId));
                $block->addButton('cancel_os_carbono', array(
                    'label' => $label,
                    'onclick' => "confirmSetLocation('{$message}', '{$url}')"
                ));
            }
        }
    }
}
