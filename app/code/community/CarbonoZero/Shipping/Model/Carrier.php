<?php

class CarbonoZero_Shipping_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * Carrier's code, as defined in parent class
     *
     * @var string
     */
    protected $_code = 'CarbonoZero';

    /**
     * Returns available shipping rates for CarbonoZero Shipping carrier
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $apiUrl = $this->getConfigData('api_url');
        $apiKey = $this->getConfigData('api_key');
        if (!isset($apiKey) || $apiKey == '') {
            return;
        }

        $destCountry = $request->getDestCountryId();
        $destRegion = $request->getDestRegionCode();
        $destCity = $request->getDestCity();
        $destStreet = $request->getDestStreet();
        if (isset($destStreet)) {
            $destStreet = $this->limparEndereco($destStreet);
        }
        $destPostcode = $request->getDestPostcode();

        $enderecoOrigem = $this->obterEnderecoOrigem();
        $cepOrigem = $this->obterCEPOrigem();

        $enderecoDestino = $this->formatarEndereco($destStreet, $destCity, $destRegion, $destCountry);
        $cepDestino = $destPostcode;

        $weight = $request->getPackageWeight();

        $ordemServico = array(
            "Tipo" => "Entregas",
            "DataServico" => date('Y-m-d'),
            "Depositos" => array (
                array (
                    "Identificador" => "1",
                    "Endereco" => $enderecoOrigem,
                    "CEP" => $cepOrigem
                )
            ),
            "Etapas" => array(
                array (
                    "Identificador" => "1",
                    "IdentificadorDeposito" => "1",
                    "Endereco" => $enderecoDestino,
                    "CEP" => $cepDestino,
                    "PesoKg"=> $weight
                )
            )
        );

        $client = new Zend_Http_Client("{$apiUrl}/v1/OrdemServico?chave_api={$apiKey}&simulacao=true");

        $json = Mage::helper('core')->jsonEncode($ordemServico);

        $client->setRawData($json, 'application/json');

        Mage::log("Collecting rates: $json", null, "CarbonoZero.log", true);

        $response = $client->request('POST');
        $status = $response->getStatus();
        $body = $response->getBody();
        if (!($status >= 200 && $status < 300)) {
            Mage::log("Failed to collect rates: {$response->getHeadersAsString()} {$body}", null, "CarbonoZero.log", true);
            return;
        }        

        Mage::log("Received rates: $body", null, "CarbonoZero.log", true);
        
        $ordemServicoRetornada = Mage::helper('core')->jsonDecode($body);

        $valor = $ordemServicoRetornada["valor"];

        
        $result = Mage::getModel('shipping/rate_result');

        $rate = Mage::getModel('shipping/rate_result_method');

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('express');
        $rate->setMethodTitle('Entrega expressa');
        $rate->setPrice($valor);
        $rate->setCost(0);
        $result->append($rate);

        return $result;
    }

    public function createShipment($order, $shippingMethod)
    {
        $apiUrl = $this->getConfigData('api_url');
        $apiKey = $this->getConfigData('api_key');
        if (strlen($apiKey) == 0) {
            $message = Mage::helper('CarbonoZero_Shipping')->__('Api Key not configured');
            throw new Exception($message);
        }

        $customerId = $order->getCustomerId();
        $customerName = $order->getCustomerName();
        $customerEmail = $order->getCustomerEmail();
        $billingAddress = $order->getBillingAddress();
        $customerTelefone = $billingAddress->getTelephone();

        $shippingAddress = $order->getShippingAddress();
        $destCountry = $shippingAddress->getCountryId();
        $destRegion = $shippingAddress->getRegionCode();
        $destCity = $shippingAddress->getCity();
        $destStreet = $shippingAddress->getStreetFull();
        if (isset($destStreet)) {
            $destStreet = $this->limparEndereco($destStreet);
        }
        $destPostcode = $shippingAddress->getPostcode();

        $enderecoOrigem = $this->obterEnderecoOrigem();
        $cepOrigem = $this->obterCEPOrigem();
        
        $enderecoDestino = $this->formatarEndereco($destStreet, $destCity, $destRegion, $destCountry);
        $cepDestino = $destPostcode;

        $destinatario = $shippingAddress->getName();
        $telefoneDestinatario = $shippingAddress->getTelephone();

        $shipment = $order->prepareShipment();
        $shipment->save();

        $identificador = "MAGE_{$order->getIncrementId()}_{$shipment->getIncrementId()}";

        $observacoes = "Pedido: {$order->getIncrementId()} | Entrega: {$shipment->getIncrementId()}";

        $weight = $order->getWeight();

        $ordemServico = array(
            "Identificador" => $identificador,
            "Tipo" => "Entregas",
            "DataServico" => date('Y-m-d'),
            "Contato" => array(
                "Identificador" => $customerId,
                "Nome" => $customerName,
                "Telefone" => $customerTelefone,
                "Email" => $customerEmail,
                "Ativo" => $customerId != null
            ),
            "Depositos" => array (
                array (
                    "Identificador" => "1",
                    "Endereco" => $enderecoOrigem,
                    "CEP" => $cepOrigem
                )
            ),
            "Etapas" => array(
                array (
                    "Identificador" => $identificador,
                    "IdentificadorDeposito" => "1",
                    "Endereco" => $enderecoDestino,
                    "CEP" => $cepDestino,
                    "PesoKg"=> $weight,
                    "Destinatario"=> $destinatario,
                    "Telefone"=> $telefoneDestinatario,
                    "Observacoes" => $observacoes
                )
            ),
        );

        $client = new Zend_Http_Client("{$apiUrl}/v1/OrdemServico?chave_api={$apiKey}");

        $json = Mage::helper('core')->jsonEncode($ordemServico);
        $client->setRawData($json, 'application/json');

        $response = $client->request('POST');

        Mage::log("Creating OS: $json", null, "CarbonoZero.log", true);

        $status = $response->getStatus();
        $body = $response->getBody();
        if (!($status >= 200 && $status < 300)) {
            Mage::log("Failed to create OS: {$response->getHeadersAsString()} {$body}", null, "CarbonoZero.log", true);
            $message = Mage::helper('CarbonoZero_Shipping')->__('Unexpected status code %s', $status);
            throw new Exception($message);
        }

        $body = $response->getBody();

        Mage::log("Created OS: $body", null, "CarbonoZero.log", true);
        
        $osRetornada = Mage::helper('core')->jsonDecode($body);
        $codigoRastreioEntrega = $osRetornada["etapas"][0]["codigoRastreio"];

        $track = Mage::getModel('sales/order_shipment_track')
            ->setNumber($codigoRastreioEntrega)
            ->setCarrierCode($this->_code)
            ->setTitle('Carbono Zero');

        $shipment->addTrack($track)->save();

        $state = $order->getState();
        if ($state == 'new' || $state == 'pending') {
            $comment = Mage::helper('CarbonoZero_Shipping')->__('CarbonoZero - shipping order created');
            $order->setState('processing', true, $comment);
            $order->save();
        }

        return $shipment;
    }

    public function cancelShipment($shipment)
    {
        $apiUrl = $this->getConfigData('api_url');
        $apiKey = $this->getConfigData('api_key');
        if (strlen($apiKey) == 0) {
            $message = Mage::helper('CarbonoZero_Shipping')->__('Api Key not configured');
            throw new Exception($message);
        }

        $order = $shipment->getOrder();

        $identificador = "MAGE_{$order->getIncrementId()}_{$shipment->getIncrementId()}";

        $client = new Zend_Http_Client("{$apiUrl}/v1/OrdemServico/{$identificador}/cancelar?chave_api={$apiKey}");

        $response = $client->request('POST');
        $status = $response->getStatus();
        $body = $response->getBody();
        if (!($status >= 200 && $status < 300)) {
            Mage::log("Failed to cancel OS: {$response->getHeadersAsString()} {$body}", null, "CarbonoZero.log", true);
            $message = Mage::helper('CarbonoZero_Shipping')->__('Unexpected status code %s', $status);
            if ($status == 422) {
                $errorDetails = Mage::helper('core')->jsonDecode($body);
                if (isset($errorDetails['errorMessage'])) {
                    $message = $errorDetails['errorMessage'];
                }
            }
            throw new Exception($message);
        }

        $comment = Mage::helper('CarbonoZero_Shipping')->__('Cancelled shipping order');
        $shipment->addComment($comment);
        $shipment->save();
    }

    /**
     * Check if current carrier offer support to tracking
     *
     * @return bool true
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    public function getTrackingInfo($trackNumber)
    {
        $trackUrlTemplate = $this->getConfigData('track_url_template');
        $trackUrl = str_replace("{code}", $trackNumber, $trackUrlTemplate);

        $tracking = Mage::getModel('shipping/tracking_result_status');
        $tracking
            ->setUrl($trackUrl)
            ->setTracking($trackNumber)
            ->setCarrier($this->_code)
            ->setCarrierTitle($this->getConfigData('title'))
            ->AddData(array('progressdetail' => array()));
            
        return $tracking;
    }

    /**
     * Returns Allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array(
            'express' => 'Entrega expressa'
        );
    }

    private function obterEnderecoOrigem() {
        $origCountry = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_COUNTRY_ID, $this->getStore());
        $origRegion = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_REGION_ID, $this->getStore());
        $origCity = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_CITY, $this->getStore());
        $origAddress1 = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_ADDRESS1, $this->getStore());
        $origAddress2 = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_ADDRESS2, $this->getStore());
        
        return $this->formatarEndereco($origAddress1, $origCity, $origRegion, $origCountry);
    }

    private function obterCEPOrigem() {
        $origPostcode = Mage::getStoreConfig(Mage_Shipping_Model_Shipping::XML_PATH_STORE_ZIP, $this->getStore());
        return $origPostcode;
    }

    private function limparEndereco($endereco) {
        return preg_replace("/[\r\n]+/", ", ", trim($endereco));
    }

    private function formatarEndereco(
        $street,
        $city,
        $state,
        $country)
    {
        return implode(
            ", ",
            array_filter(
                array($street, $city, $state, $country),
                function($value) { return isset($value) && trim($value) !==''; }
            )
        );
    }
}