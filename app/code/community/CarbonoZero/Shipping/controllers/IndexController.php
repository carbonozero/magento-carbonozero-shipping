<?php

class CarbonoZero_Shipping_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function createOSAction()
    {
        try {
            $orderId = $this->getRequest()->getParam('order_id');

            $order = Mage::getModel('sales/order')->load($orderId);
            
            $shippingCarrier = $order->getShippingCarrier();
            $shippingMethod = $order->getShippingMethod();
            $shipment = null;

            if ($order->hasShipments())
                throw new Exception('Order already has shipment');   

            if (!method_exists($shippingCarrier, "createShipment"))
                throw new Exception('Method createShipment not found');

            $shipment = $shippingCarrier->createShipment($order, $shippingMethod);
            
            $message = Mage::helper('CarbonoZero_Shipping')->__('Shipping order created');
            $this->_getSession()->addSuccess($message);
        } catch (Exception $e) {
            $message = Mage::helper('CarbonoZero_Shipping')->__('Failed to create shipping order');
            $this->_getSession()->addError("{$message}: {$e->getMessage()}");
        }

        $redirectUrl = Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=>$orderId));
        Mage::app()->getResponse()->setRedirect($redirectUrl);
    }

    public function cancelOSAction()
    {
        try {
            $shipmentId = $this->getRequest()->getParam('shipment_id');

            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
            $order = $shipment->getOrder();
            
            $shippingCarrier = $order->getShippingCarrier();
            $shippingMethod = $order->getShippingMethod();

            if (!method_exists($shippingCarrier, "cancelShipment"))
                throw new Exception('Method cancelShipment not found');

            $shippingCarrier->cancelShipment($shipment);

            $message = Mage::helper('CarbonoZero_Shipping')->__('Shipping order cancelled');
            $this->_getSession()->addSuccess($message);
        } catch (Exception $e) {
            $this->_getSession()->addError("Falha ao cancelar OS: {$e->getMessage()}");
        }

        $redirectUrl = Mage::helper('adminhtml')->getUrl("adminhtml/sales_shipment/view", array('shipment_id'=>$shipmentId));
        Mage::app()->getResponse()->setRedirect($redirectUrl);
    }
}
